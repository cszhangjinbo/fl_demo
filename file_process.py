import fileinput

for line in fileinput.FileInput('adult.test.csv', inplace=True):
    line = line[:-2]
    line = line.replace(', ', ',')
    print(line, end='\n')
fileinput.close()
